# itcast_netty

#### 介绍
黑马netty学习源码

自己学习的一些netty源码：protobuf方式等


singular：一个格式良好的消息应该有0个或者1个这种字段（但是不能超过1个）
```shell
   【比如生成java实例时候，字段域不设置值会使用默认值】
    类型	默认值
    string	空字符串
    bytes	空byte数组
    bool	false
    数值类型	0
    enums	定义的枚举第一个元素（默认必须为0
```
repeated：在一个格式良好的消息中，这种字段可以重复任意多次（包括0次）。重复的值的顺序会被保留。
```shell
   【比如生成java实例时候，字段域就是一个集合列表】
```

--https://blog.csdn.net/xiaoma_bk/article/details/101365192
--https://juejin.cn/post/6844903984914759693

![输入图片说明](LengthFieldBasedFrameDecoder.jpg)